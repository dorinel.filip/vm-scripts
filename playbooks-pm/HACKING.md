# Changing the avr toolchain

If you want to change the avr-gcc build, create a new machine (without any avr-gcc, avr-binutils, avr-libc) and run the `dev-scripts/build_debs.sh` script.

Then copy the generated deb files into the `resources` folder and adjust the ansible yml files.
