#!/bin/bash

set -e

VM_NAME='pm-vm'
OUTPUT_PATH='pm-vm-1.0.ova'

vboxmanage storageattach $VM_NAME --storagectl SCSI --port 1 --medium none
vboxmanage sharedfolder remove $VM_NAME --name vagrant
vboxmanage export $VM_NAME --ovf10 -o $OUTPUT_PATH

md5sum $OUTPUT_PATH
sha1sum $OUTPUT_PATH

