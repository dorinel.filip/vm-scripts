#!/bin/bash

vboxmanage modifyvm  'pp-vm-lite' --uart1 off
vboxmanage storageattach 'pp-vm-lite' --storagectl SCSI --port 1 --medium none
vboxmanage sharedfolder remove 'pp-vm-lite' --name vagrant
vboxmanage export 'pp-vm-lite' --ovf10 -o pp-vm-lite-1.1.ova
